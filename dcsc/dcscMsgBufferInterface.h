
/************************************************************************
**
**
** This file is property of and copyright by the Experimental Nuclear 
** Physics Group, Dep. of Physics and Technology
** University of Bergen, Norway, 2004
** This file has been written by Matthias Richter,
** Matthias.Richter@ift.uib.no
**
** Permission to use, copy, modify and distribute this software and its  
** documentation strictly for non-commercial purposes is hereby granted  
** without fee, provided that the above copyright notice appears in all  
** copies and that both the copyright notice and this permission notice  
** appear in the supporting documentation. The authors make no claims    
** about the suitability of this software for any purpose. It is         
** provided "as is" without express or implied warranty.                 
**
*************************************************************************/

#ifndef __DCSC_RCU_ACCESS_H
#define __DCSC_RCU_ACCESS_H
#ifdef __cplusplus
#define __DCSC_CXX_START extern "C" { 
#define __DCSC_CXX_END }
#else
#define __DCSC_CXX_START 
#define __DCSC_CXX_END 
#endif
__DCSC_CXX_START

/* 
This file implements the basic access function to dcs board message buffer interface via the 
dcsdriver. The interface consists of three buffers: the message in buffer (MIB), the message 
out or result buffer (MRB) and the register buffer (REG). The driver treats this buffers as 
ordinary memory in the address space of the arm linux. It exports the three buffers in a row, 
no matter where they are physically located in the address space. The size of each of the three 
buffers can be requested by the ioctl function (see below).


format of the driver access space:
==================================
     address        buffer
0                 : MIB
size of MIB       : MRB
size of MIB + MRB : REG

The  dcscMsgBufferInterface interface query each buffer size at initialization and adapts its access to 
that values. The size is always in bytes, nevertheless the MIB and MRB are treated as 32 bit memory 
and the REG as 8 bit memory (NB! there was some problems with register access concerning this 32/8 bit 
question, but this affects only the registers other than 0).

By now only the first register is used:
bit 7: execute flag
bit 6: multiplexer switch for the MIB re-read
bit 0: command ready (foreseen, but not yet implemented)
refer to the dcscMsgBufferInterface.h file for the defines


Generall access sequence
========================
To access memory location inside the RCU the following process is invoked:
1. a sequence of command blocks is written to the MIB
2. the 'execute' bit in the control register is set
3. the dcs message buffer interface interprets and executes the command sequence
4. the result (containing status word and data) is placed in the MRB
5. the 'execute' bit is cleared
6. result is interpreted
step 1,2 and 6 is done by the dcscMsgBufferInterface interface and the dcsdriver
step 3-5 is done by the arm processor firmware


Format of the command sequence (32 bit words):
==============================================
version 1:
1. : information word
2. : data word #0
n+1: data word #n
n+2: block marker (bit 16-31: AA55) and check sum (not yet implemented)
      :
x  : end marker (bit 16-31: DD3)

Words 1 (information word) to n+2 (block marker) are concerned as programming blocks. A sequence 
of an arbitrary number of programming blocks is terminated by the end marker word. This is only 
limited by the size of the MIB. By now the dcscMsgBufferInterface interface uses only one programming block 
per sequence.

Format of the information word:
Bit 0 - 15: number of words in the programming block including both information and marker word
Bit 28- 31: command id
*/
#define SINGLE_READ	0x1
#define SINGLE_WRITE	0x2
#define MULTI_READ	0x3
#define MULTI_WRITE	0x4
#define RANDOM_READ	0x5 /*  not yet implemented in dcscMsgBufferInterface but provided by the firmware */
#define RANDOM_WRITE	0x6 /*  not yet implemented in dcscMsgBufferInterface but provided by the firmware */
/* commands specific to the rcu flash access, available since version 2.2. of the DCSboard firmware  
 */
#define FLASH_ERASEALL    0x21 /*  erase the flash completely, no data words */
#define FLASH_ERASE_SEC   0x22 /*  erase one sector of the flash, 1 data word */
                               /*  sector address  */
#define FLASH_MULTI_ERASE 0x24 /*  erase multiple sectors of the flash, 2 data words */
                               /*  sector address and count */
#define FLASH_READID      0x28 /*  read the ID of the flash, 1 data word */
                               /*  0 manufacturer id, 1 device id */
#define FLASH_RESET       0x30 /*  reset the flash, no data words */
/*

changes in version 2:
Bit 0-5  : command id
Bit 6-15 : number of words(excluding the information and marker words)
Bit 16-23: block number
Bit 24-25: data format
Bit 26   : flash mode if set

the firmware has an inbuilt check for Bit 28-31, only 0x0 or 0x8 to 0xf are allowd for the 4 MSBs 

Message Buffer Header Format Version
====================================
Bit 28-31 of message buffer header
0x0     : not used
0x1-0x7 : version 1 (bit 31==0, other bits arbitrary)
0xa     : version 2
0xf     : Feeserver CE command
*/
#define MSGBUF_VERSION_1_MASK 0x70000000
#define MSGBUF_VERSION_2      0xa0000000 /*  from version 2.0 */
#define MSGBUF_VERSION_2_2    0xb0000000 /*  from version 2.2 */
#define FEESERVER_CMD         0xf0000000
/*

Format of the command result (32 bit words):
============================================
1.information word (same format as above)
2.status word
3.- n: the data words follow
Bit 0-15 of the information word contain the number of words in the buffer including status and information word. There is no end marker

Status word: = 0 if no error
Bit 15: set if any error
*/
#define MSGBUF_STATUS_NOMARKER  0x01/* Bit 0: missing marker */
#define MSGBUF_STATUS_NOENDMRK  0x02/* Bit 1: missing end marker */
#define MSGBUF_STATUS_NOTGTASW  0x04/* Bit 2: no target answer (something wrong with the RCU or not connected) */
#define MSGBUF_STATUS_NOBUSGRT  0x08/* Bit 3: no bus grant (no access to the bus on dcs board) */
#define MSGBUF_STATUS_OLDFORMAT 0X10/* Bit 5: old message buffer format (prior to v2 with rcu-sh version v1.0) */
/*

 */
#include <linux/types.h> /*  for the __u32 type */

/*
 * program debug options
 */
#define PRINT_COMMAND_BUFFER        0x01 /*  print the command buffer before writing to message in buffer */
#define PRINT_RESULT_BUFFER         0x02 /*  print the message out buffer after executing the command */
#define CHECK_COMMAND_BUFFER        0x04 /*  test the message in buffer after writing the command buffer to it */
#define IGNORE_BUFFER_CHECK         0x08 /*  ignore the result of the read back check of the message in buffer */
#define PRINT_REGISTER_ACCESS       0x10 /*  print access to the control registers */
#define PRINT_COMMAND_RESULT        0x20 /*  print the result of the command */
#define PRINT_SPLIT_DEBUG           0x40 /*  print split info for multiple operations */
#define DBG_FILE_CONVERT            0x80 /*  print debug information on the file conversion */
#define PRINT_RESULT_HUMAN_READABLE 0x100 /*  print the meaning of the status bits */
/* defined in mrshellprim.h now #define DBG_ARGUMENT_CONVERT        0x200 // print debug information concerning argument conversion */
#define DBG_CHECK_COMMAND           0x400 /*  print debug information concerning the wait ('check') command */

#define DBG_DEFAULT                 PRINT_RESULT_HUMAN_READABLE

/* additional error codes to the ones specified in errno.h
 *
 */
#define EMISS_MARKER         0x1001
#define EMISS_ENDMARKER      0x1002
#define ENOTARGET            0x1003
#define ENOBUSGRANT          0x1004
#define EOLDFORMAT           0x1005

/************************************************************************************************************
 * provided methods
 */

/************************************************************************************************************
 * initialization and general methods
 */

/* init the interface, the device will be opened and some other other internal states
 * initialized
 * parameter:
 *   pDeviceName: name of the device node, if NULL: /dev/dcsc as default
 * result:
 *   -ENOSPC : can not get the size of the interface buffers, or buffers too small
 *   -ENOENT : can not open device
 */
int initRcuAccess(const char* pDeviceName);

/* argument struct for extended initialization
 */
struct dcscInitArguments_t {
  unsigned int flags;  
  int iVerbosity;
  int iMIBSize;      /*  size of the MIB */
};
typedef struct  dcscInitArguments_t TdcscInitArguments;

/* initialization flags
 */
#define DCSC_INIT_FORCE_V1 0x0001 /*  force message buffer format version v1 */
#define DCSC_INIT_FORCE_V2 0x0002 /*  force message buffer format version v2 */
#define DCSC_INIT_ENCODE   0x0100 /*  write block to device/file but do not execute */
#define DCSC_INIT_APPEND   0x0200 /*  append to the file */
#define DCSC_SUPPR_STDERR  0x0400 /*  suppress stderr output */
#define DCSC_SKIP_DRV_ADPT 0x1000 /*  skip automatic adaption to driver properties (MIB size, version , ...)  */

/* extended initialization
 * parameter:
 *   pDeviceName: name of the device node, if NULL: /dev/dcsc as default
 *   flags: logical or of the following flags
 *   iMIBSize: size of the MIB, used for encoding of message blocks
 * result:
 *   -ENOSPC : can not get the size of the interface buffers, or buffers too small
 *   -ENOENT : can not open device
 */
int initRcuAccessExt(const char* pDeviceName, TdcscInitArguments* pArg);

/* this will basically close the device and release internal data structures
 * result:
 *   -ENXIO : no device open
 */
int releaseRcuAccess();

/* a debug functionality for simulation of the firmware response is implemented. Instead reading
 * the control register (reg 0), a text file will be read. This helps when testing the software
 * on another machine than the dcs board or when firmware is under development
 * a comprehensive description will follow later
 *
 * the simulation feature has to be turned on at compile time with the option 'dcscsim=yes'
 *
 * start the simulation
 */
void startSimulation();

/* stop the simulation
 */
void stopSimulation();

/* reset the simulation
 * reset all the internal variables and seek to beginning of the register files
 */
void resetSimulation();

/* DEBUG messages
 *
 * set the debug options, refer to the help menu
 * return: current value of options
 */
int setDebugOptions(int options);

/* set a debug option flag, refer to the help menu
 * return: current value of options
 */
int setDebugOptionFlag(int of);

/* clear a debug option flag, refer to the help menu
 * return: current value of options
 */
int clearDebugOptionFlag(int of);

/* DEBUG helper functions
 * 
 * print content of a buffer hexadecimal, the content is assumde to be little endian (LSB first)
 * parameters:
 *   pBuffer     : pointer to buffer
 *   iBufferSize : size of the buffer in byte
 *   iWordSize   : number of bytes in one word (1,2 or 4)
 *   pMessage    : an optional message
 */
void printBufferHex(unsigned char *pBuffer, int iBufferSize, int wordSize, const char* pMessage);

/*
 * print content of a buffer hexadecimal and formated with the address, 
 * the content is assumde to be little endian (LSB first)
 * parameters:
 *   pBuffer     : pointer to buffer
 *   iBufferSize : size of the buffer in byte
 *   iWordSize   : number of bytes in one word (1,2 or 4)
 *   iWordsPerRow: number of words in one row
 *   iStartAddress: start address for the output
 *   pMessage    : an optional message
 */
void printBufferHexFormatted(unsigned char *pBuffer, int iBufferSize, int iWordSize, int iWordsPerRow, int iStartAddress, const char* pMessage);



/* get driver info from the driver and print it
 * parameter:
 *   iVerbosity - 1 full text, 0 just a few messeges
 */
void printDriverInfo(int iVerbosity);

/************************************************************************************************************
 * read/write access
 */

/* write a single location (32bit word)
 *  address - start location
 *  data - data word
 * result:
 */
int rcuSingleWrite(__u32 address, __u32 data);

/* read a single location (32bit word)
 *  address - start location
 *  pData - buffer to receive the data
 * result:
 */
int rcuSingleRead(__u32 address, __u32* pData);

/* write a number of 32bit words beginning at a location
 * the function takes care for the size of the MIB and splits the operation if the amount of data to write 
 * exceeds the MIB size
 * the function expects data in little endian byte order
 * parameter:
 *  address - start location
 *  pData - buffer containing the data
 *  iSize - number of words to write
 *  iDataSize - size of one word in bytes, allowed 1 (8 bit), 2 (16 bit),3 (compressed 10 bit) or 4 (32 bit)
 *              -2 and -4 denote 16/32 bit words which get swapped
 * result:
 */
int rcuMultipleWrite(__u32 address, __u32* pData, int iSize, int iDataSize);

/* read a number of 32bit words beginning at a location
 * parameter:
 *  address - start location
 *  iSize - number of words to read
 *  pData - buffer to receive the data, the function expect it to be of suitable size (i.e. iSize x wordsize)
 * result: no of read 32bit words
 *  <0 standard error code
 */
int rcuMultipleRead(__u32 address, int iSize,__u32* pData);

/* provide the message buffer for direct access
 * creation and destruction of the buffer is handled by the interface internally
 * DO NOT DELETE THIS BUFFER
 * parameter:
 *   ppBuffer - target to receive the buffer pointer
 *   pSize - target to receive the buffer size
 * result:
 *   pointer to buffer, the caller is supposed to write all the information word, the markers and
 *   data as well as check sum, the interface does not alter it
 */
int dcscProvideMessageBuffer(__u32** ppBuffer, int* pSize);

/* prepare the message buffer for a specified command
 * creation and destruction of the buffer is handled by the interface internally
 * DO NOT DELETE THIS BUFFER
 * parameter:
 *   ppBuffer - target to receive the buffer pointer
 *   pSize - target to receive the buffer size
 *   cmdID - command ID to prepare the buffer for
 *   flags - for future extensions (e.g. preparation for compressed pedestal data)
 * result:
 *   pointer to buffer where the data words can directly be stored to, the information word and the markers
 *   and check sum are written by the interface
 */
int dcscPrepareMessageBuffer(__u32** ppBuffer, int* pSize, unsigned int cmdID, unsigned int flags);

/* execute the command message, pointer and size are reuired in order to cross check the buffer
 * arguments:
 *   pCommand - pointer to command buffer to execute
 *   iSize - size of the command buffer
 *   ppTarget - target to receive the result buffer (e.g. for read operations)
 *   ppTargetSize - target to receive the size of the result buffer
 *   operation - flags for operation control (for future extension) 
 *               this tool is going to be used also for the encoding of configuration data, 
 *               it might be usefull to write the ready prepared command buffer to a file or
 *               pipe. In that sense no execution and result interpretation is needed, this
 *               will be controlled by the operation flags
 * result:
 *   result buffer if available
 */
int dcscExecuteCommand(__u32* pBuffer, int iSize, __u32** ppTarget, int* ppTargetSize, unsigned int operation);

enum {
  eUnknownSpec = 0,
  eVersion,
  eCommand,
  eNofWords,
  ePacked
};
/* extract part of the header word according to the specifier
 */
int dcscGetHeaderAttribute(__u32 header, int iSpecifier);

/* check a message block for correct format
 * arguments:
 *   pBuffer - pointer to message block
 *   iSize - size of the block in 32bit words
 *   iVerbosity - 0: no messages, 1: warnings for format missmatch, 2: debug messages
 * result:
 *   >0 check successful, return block length in 32bit words
 *   =0 unknown format
 *   <0 other errors
 */
int dcscCheckMsgBlock(__u32* pBuffer, int iSize, int iVerbosity);

/************************************************************************************************************
 * driver control functions
 */

enum {
  eUnknownDriverCmd=0,
  eLock,
  eUnlock,
  eDeactivateLock,
  eActivateLock
};

/* lock the driver
 */
int dcscLockCtrl(int cmd);

/* set debug flags for the driver
 */
int dcscDriverDebug(unsigned int flags);

/************************************************************************************************************
 * bus control functions
 */

/* command ids to the rcuBusControlCmd function
 */
enum {
  eUnknownBusCtrl=0,
  eEnableSelectmap,  /*  enable the select map mode, interface has to be in msg buffer mode */
  eDisableSelectmap,
  eEnableFlash,      /*  enable the flash mode, interface has to be in msg buffer mode */
  eDisableFlash,
  eEnableMsgBuf,     /*  disable both select map and flash mode */
  eResetFirmware,    /*  trigger a firmware reset sequence */
  eReadCtrlReg,      /*  read the value of the control register */
  eCheckSelectmap,   /*  check whether in mode selectmap */
  eCheckFlash,       /*  check whether in mode flash */
  eCheckMsgBuf,      /*  check whether in mode msg buffer */
  eResetFlash,       /*  send reset command to flash */
  eFlashID,          /*  read the flash id */
  eFlashCtrlDCS,     /*  all flash control done by the DCS board FW, since FW version 2.2 */
  eFlashCtrlActel,   /*  all flash control done by the RCU Actel FW */
  eEnableCompression,/*  use compressed data in msg buffer whenever possible */
  eDisableCompression
};

/* switch bits in the control register (firmware comstat)
 * possible commands:
 *  see enums above
 * result: the (new) value of the control register
 *  -EBADFD if interface in wrong state to perform the command
 *  -EIO    if internal error (bits can not be changed)
 *  -EINVAL unknown command id
 */
int rcuBusControlCmd(int iCmd);

/************************************************************************************************************
 * flash control functions
 */

/* write to the RCU flash 
 * currently all communication is done through the message buffer
 * parameter:
 *  address - start location
 *  pData - buffer containing the data
 *  iSize - number of words to write
 *  iDataSize - size of one word in bytes, allowed 1,2
 * result:
 */
int rcuFlashWrite(__u32 address, __u32* pData, int iSize, int iDataSize);

/* read a number of 16bit words from the flash memory
 * currently all communication is done through the message buffer
 * parameter:
 *  address - start location
 *  iSize - number of words to read
 *  pData - buffer to receive the data, the function expect it to be of suitable size (i.e. iSize x wordsize)
 * result: no of read 32bit words
 *  <0 standard error code
 */
int rcuFlashRead(__u32 address, int iSize,__u32* pData);

/* erase sectors of the flash
 * parameter:
 *   firstSec - the first sector to erase; if -1 erase all
 *   lastSec - the last sector to erase
 */
int rcuFlashErase(int startSec, int stopSec);

__DCSC_CXX_END
#endif /* __DCSC_RCU_ACCESS_H */
