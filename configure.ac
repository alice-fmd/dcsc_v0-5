dnl -*- mode: autoconf -*- 
dnl
dnl $Id: configure.ac,v 1.6 2012-03-07 08:14:57 cholm Exp $
dnl template for the configuration script for the Dcsc
dnl with RCU-like ControlEngines
dnl 
dnl ------------------------------------------------------------------
AC_INIT([DCS Message buffer], [0.5], [Matthias.Richter@ift.uib.no], dcsc)
AC_CONFIG_AUX_DIR(config)
AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_SRCDIR(dcsc/dcscMsgBufferInterface.c)
AC_CANONICAL_SYSTEM
AC_PREFIX_DEFAULT(${PWD})
AM_INIT_AUTOMAKE
AC_PROG_CC
dnl AC_PROG_CXX
AC_PROG_LIBTOOL
dnl AC_PROG_RANLIB
sovers=1
AC_SUBST(sovers)

AC_DEBUG
AC_OPTIMIZATION
AC_STRICT

dnl the ARM_COMPILING switch is used in the dcsc/Makefile.am
dnl to set the linkink option to libtool (-all-static)
dnl so far I couldn't find out how to do this on the configure 
dnl command line. Although this flag should be set according to
dnl users choice of --anable/disable-shared
if test "x$cross_compiling" = "xyes" ; then
 case "${host}" in
 arm*)arm_compiling=yes ;;
 *)   arm_compiling=no ;; 
 esac
fi
AC_MSG_CHECKING(whether we're compiling for arm-linux)
AM_CONDITIONAL(ARM_COMPILING, test x$arm_compiling = xyes )
AC_MSG_RESULT($arm_compiling)

dnl ------------------------------------------------------------------
AC_MSG_CHECKING([whether to enable DCSC simulation])
AH_TEMPLATE([DCSC_SIMULATION],[Simulated DCS])
AC_ARG_ENABLE(dcscsim,
  [AC_HELP_STRING([--enable-dcscsim],
      [Turn on register simulation feature for Msg Buffer Interface ])],
  [],[enable_dcscsim=no])
if test "x$enable_dcscsim" = "xyes" ; then 
  AC_DEFINE(DCSC_SIMULATION)
fi
# AM_CONDITIONAL(DCSCSIM, test x$dcscsim = xtrue)
AC_MSG_RESULT([$enable_dcscsim])

dnl ------------------------------------------------------------------
dnl thread flags for the dcsc
dnl AC_THREAD_FLAGS

dnl ------------------------------------------------------------------
dnl
dnl Documentation
dnl
AC_ARG_VAR(DOXYGEN, The Documentation Generator)
AC_PATH_PROG(PERL, perl)
AC_PATH_PROG(DOXYGEN, doxygen)
AC_ARG_ENABLE(docs,
  [AC_HELP_STRING([--enable-docs],
      [Turn on generation of documentation])],
  [],[enable_docs=no])
if test "x$enable_docs" != "xyes" ; then DOXYGEN= ; fi
AM_CONDITIONAL(HAVE_DOXYGEN, test ! "x$DOXYGEN" = "x")
AC_MSG_CHECKING(whether we should do the documentation)
AC_MSG_RESULT($enable_docs)
HAVE_DOT=NO
DOT_PATH=
AC_PATH_PROG(DOT, dot)
if ! test "x$DOT" = "x" ; then
   HAVE_DOT=YES
   DOT_PATH=`dirname $DOT`
fi
AC_SUBST([HAVE_DOT])
AC_SUBST([DOT_PATH])

dnl ------------------------------------------------------------------
dnl
dnl RPM stuff 
dnl
RPM_VERSION=`echo $VERSION | tr '-' '_'`

AC_SUBST(RPM_VERSION)
AC_SUBST(RPM_PREFIX)
AC_SUBST(RPM_BUILD_ARGS)

dnl ------------------------------------------------------------------
AC_CONFIG_FILES([Makefile 
		 dcsc/Makefile
		 doc/Makefile
		 doc/doxygen.conf
		 support/dcsc.spec
		 support/Makefile
		 support/libdcsc.pc
		 support/dcsc-config
		 debian/Makefile
		 debian/libdcsc${sovers}.dirs:debian/libdcsc.dirs.in
		 debian/libdcsc${sovers}.install:debian/libdcsc.install.in])

AC_OUTPUT
dnl
dnl EOF
dnl

